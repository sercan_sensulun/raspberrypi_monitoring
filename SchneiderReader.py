import serial
import minimalmodbus
import time
import os
#from pymodbus3.client.sync import ModbusTcpClient


class SchneiderRegister:
	def __init__(self,title,address,datatype):
		self.title = title
		self.address = address
		self.datatype = datatype

def readFromFile(path):
	schneiderSheet_file = open(path,'r')
	lines = schneiderSheet_file.readlines();
	schneiderRegisters = []
	for line in lines: 
		schneiderRegisterObjectAreaList=line.split('|')
		scheneiderRegister = SchneiderRegister(
			title=schneiderRegisterObjectAreaList[0],
			address=int(schneiderRegisterObjectAreaList[1]),
			datatype=schneiderRegisterObjectAreaList[2])
		schneiderRegisters.append(scheneiderRegister)
	return schneiderRegisters

def readFromPM710Device(SchneiderRegisterList):
	instrument = minimalmodbus.Instrument(port='/dev/ttyAMA0',slaveaddress=1,mode=minimalmodbus.MODE_RTU)
	instrument.serial.baudrate = 9600
	instrument.debug = False
	instrument.serial.timeout = 5

	for scheneiderRegister in SchneiderRegisterList:
		value = instrument.read_float(registeraddress=scheneiderRegister.address, functioncode=3, numberOfRegisters=2)
		print(scheneiderRegister.title + " = " + str(value))

if __name__ == '__main__':
    schneiderSheetPath = os.getcwd()+'/SchneiderSheet.txt'
    while 1:
        schneiderRegistersFromFile = readFromFile(schneiderSheetPath)
        readFromPM710Device(schneiderRegistersFromFile)
	time.sleep(1)